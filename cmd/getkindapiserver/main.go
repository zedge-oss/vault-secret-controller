package main

import (
	"bytes"
	"fmt"
	"os"
	"os/exec"

	"gopkg.in/yaml.v3"
	v1 "k8s.io/client-go/tools/clientcmd/api/v1"
)

func main() {
	args := []string{"get", "kubeconfig", "--internal"}
	if len(os.Args) == 2 {
		args = append(args, "--name", os.Args[1])
	}
	cmd := exec.Command("kind", args...)
	var errbuf bytes.Buffer
	cmd.Stderr = &errbuf
	output, err := cmd.Output()
	if err != nil {
		_, _ = os.Stderr.Write(errbuf.Bytes())
		os.Exit(1)
	}
	var data v1.Config
	if err = yaml.Unmarshal(output, &data); err != nil {
		panic(err)
	}
	if len(data.Clusters) != 1 {
		_, _ = fmt.Fprintf(os.Stderr, "Error: expected one cluster, found %d\n", len(data.Clusters))
		os.Exit(1)
	}
	fmt.Println(data.Clusters[0].Cluster.Server)
}
