FROM gcr.io/distroless/static:nonroot

COPY bin/manager /manager

ENTRYPOINT "/manager"
