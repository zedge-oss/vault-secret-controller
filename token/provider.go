package token

import (
	"sync"
	"time"

	vault "github.com/hashicorp/vault/api"
)

type Provider interface {
	GetToken() (string, error)
}

type lastSecret struct {
	mu     *sync.Mutex
	secret *vault.Secret
	time   time.Time
}
