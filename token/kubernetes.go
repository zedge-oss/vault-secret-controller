package token

import (
	"fmt"
	"io/ioutil"
	"strings"
	"sync"
	"time"

	"github.com/go-logr/logr"
	vault "github.com/hashicorp/vault/api"
)

type KubernetesProvider struct {
	VaultConfig             *vault.Config
	VaultAuthLoginPath      string
	ServiceAccountTokenPath string
	VaultAuthRole           string
	Log                     logr.Logger
	lastSecret              lastSecret
}

func (p *KubernetesProvider) needsRefresh() bool {
	p.lastSecret.mu.Lock()
	defer p.lastSecret.mu.Unlock()
	l := &p.lastSecret
	if l.secret == nil || l.secret.Auth == nil {
		return true
	}
	// set the expire time 60 seconds before the actual one to give us some time to refresh
	expireTime := l.time.Add(time.Second * time.Duration(l.secret.Auth.LeaseDuration)).Add(-time.Minute)
	return time.Now().After(expireTime)
}

func NewKubernetesProvider(config *vault.Config, log logr.Logger, loginPath, role, tokenPath string) *KubernetesProvider {
	return &KubernetesProvider{
		VaultConfig:             config,
		VaultAuthLoginPath:      loginPath,
		ServiceAccountTokenPath: tokenPath,
		VaultAuthRole:           role,
		Log:                     log,
		lastSecret:              lastSecret{mu: &sync.Mutex{}},
	}
}

func (p *KubernetesProvider) getLastSecret() *vault.Secret {
	p.lastSecret.mu.Lock()
	defer p.lastSecret.mu.Unlock()
	return p.lastSecret.secret
}

func (p *KubernetesProvider) updateLastSecret(secret *vault.Secret) {
	p.lastSecret.mu.Lock()
	defer p.lastSecret.mu.Unlock()
	p.lastSecret.secret = secret
	p.lastSecret.time = time.Now()
}

func (p KubernetesProvider) GetToken() (string, error) {
	var secret *vault.Secret
	if p.needsRefresh() {
		p.Log.Info("Refreshing client token", "auth_path", p.VaultAuthLoginPath, "role", p.VaultAuthRole)
		jwtData, err := ioutil.ReadFile(p.ServiceAccountTokenPath)
		if err != nil {
			return "", err
		}
		jwt := strings.TrimSpace(string(jwtData))
		cli, err := vault.NewClient(p.VaultConfig)
		if err != nil {
			return "", err
		}
		secret, err = cli.Logical().Write(p.VaultAuthLoginPath, map[string]interface{}{
			"role": p.VaultAuthRole,
			"jwt":  jwt,
		})
		if err != nil {
			return "", fmt.Errorf("GetToken from %q: %w", p.VaultAuthLoginPath, err)
		}
		if secret.Auth == nil {
			return "", fmt.Errorf(`no "auth" in auth response`)
		}
		p.updateLastSecret(secret)
		p.Log.Info("Successfully refreshed client token", "auth_path", p.VaultAuthLoginPath, "role", p.VaultAuthRole)
	}
	return secret.Auth.ClientToken, nil
}

var _ Provider = &KubernetesProvider{}
