package token

type StaticProvider struct {
	Token string
}

func (p *StaticProvider) GetToken() (string, error) {
	return p.Token, nil
}

var _ Provider = &StaticProvider{}
