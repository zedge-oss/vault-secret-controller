#!/bin/bash

rnd="$RANDOM"
ns="test-$rnd"
crb="vault-auth-delegation-$rnd"
auth_path="kubernetes-$rnd"
policy_file="$(dirname $0)/vault-test.hcl"
role="role-$rnd"
policy="policy-$rnd"

set -e
command kubectl create ns "$ns"
trap 'set -x +e; command kubectl delete ns "$ns"; command kubectl delete clusterrolebinding "$crb"; vault auth disable "$auth_path"; vault policy delete "$policy"' EXIT
k() {
    command kubectl -n "$ns" "$@"
}
ctx="$(kubectl config current-context)"
kubeconfig="$(kind get kubeconfig --internal | yq .)"
cluster="$(echo "$kubeconfig" | jq -r --arg current "$ctx" '.contexts[]|select(.name==$current)|.context.cluster')"
ca_cert="$(echo "$kubeconfig" |
  jq --arg cluster "$cluster" -r '.clusters[]|select(.name==$cluster)|.cluster."certificate-authority-data"' |
  base64 --decode)"
server="$(echo "$kubeconfig" | jq --arg cluster "$cluster" -r '.clusters[]|select(.name==$cluster)|.cluster.server')"
#server=https://172.17.0.15:6443

k create sa vault-auth
k create clusterrolebinding "$crb" --group=rbac.authorization.k8s.io --clusterrole=system:auth-delegator --serviceaccount="$ns:vault-auth"
k create sa controller
auth_secret_name=$(k get sa vault-auth -o jsonpath='{.secrets[0].name}')
auth_token=$(k get secret "$auth_secret_name" -o jsonpath='{.data.token}' | base64 --decode)
ctrl_secret_name=$(k get sa controller -o jsonpath='{.secrets[0].name}')
ctrl_token=$(k get secret "$ctrl_secret_name" -o jsonpath='{.data.token}' | base64 --decode)
vault auth disable "$auth_path"
vault auth enable -path="$auth_path" kubernetes
vault write "auth/$auth_path/config" kubernetes_host="$server" kubernetes_ca_cert="$ca_cert" token_reviewer_jwt="$auth_token"

vault policy write "$policy" "$policy_file"
vault write "auth/$auth_path/role/$role" bound_service_account_names=controller bound_service_account_namespaces="$ns" policies="$policy" ttl=1h

vault write "auth/$auth_path/login" role="$role" jwt="$ctrl_token"

echo "> "
echo "> Running a shell, will clean up after exiting."
echo "> "

export auth_token ctrl_token auth_path rnd crb ns policy_file role
bash
