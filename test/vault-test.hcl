path "secret/metadata/test" {
  capabilities = ["list"]
}

path "secret/data/test/*" {
  capabilities = ["create", "read", "update", "delete"]
}
