package v1beta1

import (
	"encoding/json"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type VaultKeyValue struct {
	// Where the secret engine is mounted (defaults to the first path element)
	Mount string `json:"mount,omitempty"`
	// Vault path
	Path string `json:"path"`
}

// VaultSecretSpec defines the desired state of VaultSecret
type VaultSecretSpec struct {
	KeyValue *VaultKeyValue    `json:"kv"`
	Type     corev1.SecretType `json:"type,omitempty"`
}

func (s *VaultSecretSpec) UnmarshalJSON(b []byte) error {
	type plain VaultSecretSpec
	if err := json.Unmarshal(b, (*plain)(s)); err != nil {
		return err
	}
	if s.Type == "" {
		s.Type = corev1.SecretTypeOpaque
	}
	return nil
}

// VaultSecretStatus defines the observed state of VaultSecret
type VaultSecretStatus struct {
}

// +kubebuilder:object:root=true

// VaultSecret is the Schema for the vaultsecrets API
type VaultSecret struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   VaultSecretSpec   `json:"spec,omitempty"`
	Status VaultSecretStatus `json:"status,omitempty"`
}

// +kubebuilder:object:root=true

// VaultSecretList contains a list of VaultSecret
type VaultSecretList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []VaultSecret `json:"items"`
}

func init() {
	SchemeBuilder.Register(&VaultSecret{}, &VaultSecretList{})
}
