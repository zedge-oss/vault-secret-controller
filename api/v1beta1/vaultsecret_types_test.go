package v1beta1

import (
	"encoding/json"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"github.com/onsi/ginkgo/reporters"
	corev1 "k8s.io/api/core/v1"
	"sigs.k8s.io/controller-runtime/pkg/envtest"
)

func TestAPIs(t *testing.T) {
	RegisterFailHandler(Fail)

	RunSpecsWithDefaultAndCustomReporters(t, "Controller Suite", []Reporter{
		envtest.NewlineReporter{},
		reporters.NewJUnitReporter("junit.xml"),
	})
}

var _ = Describe("API unmarshaller", func() {
	var data string
	var spec VaultSecretSpec
	JustBeforeEach(func() {
		Expect(json.Unmarshal([]byte(data), &spec)).To(Succeed())
	})
	When(`"type" is not specified`, func() {
		BeforeEach(func() {
			data = `{"kv":{"path":"foo"}}`
		})
		It(`it should default to "Opaque"`, func() {
			Expect(spec.Type).To(Equal(corev1.SecretTypeOpaque))
		})
	})
	When(`"type" is specified but not "Opaque"`, func() {
		BeforeEach(func() {
			data = `{"kv":{"path":"foo"},"type":"kubernetes.io/tls"}`
		})
		It(`it should not be changed`, func() {
			Expect(spec.Type).To(Equal(corev1.SecretTypeTLS))
		})
	})
	When(`"type" is "Opaque"`, func() {
		BeforeEach(func() {
			data = `{"kv":{"path":"foo"},"type":"Opaque"}`
		})
		It(`it should remain "Opaque"`, func() {
			Expect(spec.Type).To(Equal(corev1.SecretTypeOpaque))
		})
	})
})
