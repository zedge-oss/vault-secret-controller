package main

import (
	"flag"
	"os"

	vault "github.com/hashicorp/vault/api"
	"k8s.io/apimachinery/pkg/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	_ "k8s.io/client-go/plugin/pkg/client/auth"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"

	zedgev1beta1 "gitlab.com/zedge/dd/vault-secret-controller/api/v1beta1"
	"gitlab.com/zedge/dd/vault-secret-controller/controllers"
	"gitlab.com/zedge/dd/vault-secret-controller/token"
	// +kubebuilder:scaffold:imports
)

const (
	defaultServiceAccountTokenPath = "/run/secrets/kubernetes.io/serviceaccount/token"
)

var (
	scheme   = runtime.NewScheme()
	setupLog = ctrl.Log.WithName("setup")
)

func init() {
	_ = clientgoscheme.AddToScheme(scheme)

	_ = zedgev1beta1.AddToScheme(scheme)
	// +kubebuilder:scaffold:scheme
}

func main() {
	var metricsAddr string
	var enableLeaderElection bool
	var vaultAuthLoginPath string
	var serviceAccountTokenPath string
	var vaultClientCertPath string
	var vaultClientKeyPath string
	var vaultClientCAPath string
	var vaultURL string
	var vaultAuthRole string
	flag.StringVar(&metricsAddr, "metrics-addr", ":8080", "The address the metric endpoint binds to.")
	flag.BoolVar(&enableLeaderElection, "enable-leader-election", false,
		"Enable leader election for controller manager. Enabling this will ensure there is only one active controller manager.")
	flag.StringVar(&vaultURL, "vault-url", os.Getenv(vault.EnvVaultAddress),
		"URL to vault server")
	flag.StringVar(&vaultClientCertPath, "vault-client-cert-path", os.Getenv(vault.EnvVaultClientCert),
		"Path to file containing TLS client certificate")
	flag.StringVar(&vaultClientKeyPath, "vault-client-key-path", os.Getenv(vault.EnvVaultClientKey),
		"Path to file containing TLS client key")
	flag.StringVar(&vaultClientCAPath, "vault-client-ca-path", os.Getenv(vault.EnvVaultCAPath),
		"Path to file containing TLS client key")
	flag.StringVar(&vaultAuthLoginPath, "vault-auth-login-path", "auth/kubernetes/login",
		"Path in Vault used for authenticating via a Kubernetes service account (without /v1/ prefix)")
	flag.StringVar(&vaultAuthRole, "vault-auth-role", "vault-secret-controller",
		"Vault role used for authentication")
	flag.StringVar(&serviceAccountTokenPath, "service-account-token-path", defaultServiceAccountTokenPath,
		"Path to Kubernetes service account jwt/token")
	flag.Parse()

	ctrl.SetLogger(zap.New(func(o *zap.Options) {
		o.Development = true
	}))

	mgr, err := ctrl.NewManager(ctrl.GetConfigOrDie(), ctrl.Options{
		Scheme:             scheme,
		MetricsBindAddress: metricsAddr,
		LeaderElection:     enableLeaderElection,
		Port:               9443,
	})
	if err != nil {
		setupLog.Error(err, "unable to start manager")
		os.Exit(1)
	}

	vaultConfig := &vault.Config{
		Address: vaultURL,
	}
	if err = vaultConfig.ConfigureTLS(&vault.TLSConfig{
		CAPath:     vaultClientCAPath,
		ClientCert: vaultClientCertPath,
		ClientKey:  vaultClientKeyPath,
	}); err != nil {
		setupLog.Error(err, "unable to create vault config", "controller", "VaultSecret")
		os.Exit(1)
	}
	authProviderLog := ctrl.Log.WithName("vault-auth-provider")
	authProvider := token.NewKubernetesProvider(vaultConfig, authProviderLog, vaultAuthLoginPath, vaultAuthRole, serviceAccountTokenPath)

	if err = (&controllers.VaultSecretReconciler{
		Client:       mgr.GetClient(),
		Log:          ctrl.Log.WithName("controllers").WithName("VaultSecret"),
		Scheme:       mgr.GetScheme(),
		AuthProvider: authProvider,
		VaultConfig:  vaultConfig,
	}).SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "VaultSecret")
		os.Exit(1)
	}
	// +kubebuilder:scaffold:builder

	setupLog.Info("starting manager")
	if err := mgr.Start(ctrl.SetupSignalHandler()); err != nil {
		setupLog.Error(err, "problem running manager")
		os.Exit(1)
	}
}
