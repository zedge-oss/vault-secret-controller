#!/bin/bash -ex

cluster=kind
#kind create cluster --name $cluster
apiserver=$(go run ./cmd/getkindapiserver $cluster)

cd "$(dirname "$0/")/.."
export KUBECONFIG="$(kind get kubeconfig-path --name "$cluster")"
go test -v ./controllers --vault-k8s-server="$apiserver" --use-existing-cluster --run-e2e-tests
