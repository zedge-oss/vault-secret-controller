#!/bin/bash

gen_crd=config/crd/bases/zedge.io_vaultsecrets.yaml
gen_role=config/rbac/role.yaml
gen_rolebinding=config/rbac/role_binding.yaml
chart_crd=charts/vault-secret-controller/templates/crd.yaml
chart_role=charts/vault-secret-controller/templates/role.yaml
chart_rolebinding=charts/vault-secret-controller/templates/role_binding.yaml

tmp_out=$(mktemp)
trap 'rm -f $tmp_out' EXIT

set -e

(sed -n '/^apiVersion:/,/^spec:/ { x; p; }' < "$chart_crd";
 sed -n '/^spec:/,$ { p; }' < "$gen_crd") > "$tmp_out"

mv "$tmp_out" "$chart_crd"

tmp_out=$(mktemp)
sed -e 's/manager-role/{{ template "vault-secret-controller.fullname" . }}/' $gen_role > $chart_role
sed -e 's/manager-rolebinding/{{ template "vault-secret-controller.fullname" . }}/;
s/manager-role/{{ template "vault-secret-controller.fullname" . }}/;
s/^  name: default/  name: {{ template "vault-secret-controller.fullname" . }}/;
s/^  namespace: .*/  namespace: {{ .Release.Namespace }}/;' $gen_rolebinding > $chart_rolebinding
