package main

import (
	// Prevent goimports from removing these dependencies (we're using them from Makefile, not from code)
	_ "sigs.k8s.io/controller-tools/pkg/genall"
)
