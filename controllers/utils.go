package controllers

import (
	"encoding/base64"
	"flag"
	"strings"

	vault "github.com/hashicorp/vault/api"

	"gitlab.com/zedge/dd/vault-secret-controller/api/v1beta1"
)

const EncodingSecretKeyName = "__encoding"
const PlainEncoding = "plain"
const Base64Encoding = "base64"

var encodingFieldName = flag.String("encoding-field-name", EncodingSecretKeyName, `If not empty, a secret field used for describing the encoding of all the other fields (supported: "base64")`)

func convertVaultData(secret *vault.Secret) map[string][]byte {
	converted := make(map[string][]byte)
	if untyped, found := secret.Data["data"]; found {
		if data, ok := untyped.(map[string]interface{}); ok {
			decoder := getDecoderForSecret(data)
			for key, value := range data {
				if key == *encodingFieldName {
					continue
				}
				if strValue, ok := value.(string); ok {
					converted[key] = decoder(strValue)
				}
			}
		}
	}
	return converted
}

func vaultPathFromSpec(kv *v1beta1.VaultKeyValue) string {
	mount := strings.TrimPrefix(kv.Mount, "/")
	path := strings.TrimPrefix(kv.Path, "/")
	if mount == "" {
		if i := strings.Index(path, "/"); i >= 0 {
			mount = path[0:i]
			path = path[i+1:]
		}
	}
	return mount + "/data/" + path
}

func getDecoderForSecret(data map[string]interface{}) valueDecoder {
	if encoding, ok := data[*encodingFieldName]; ok {
		if encodingStr, ok := encoding.(string); ok {
			switch encodingStr {
			case Base64Encoding:
				return decodeBase64String
			case PlainEncoding:
				return decodePlainString
			}
		}
	}
	return decodePlainString
}

type valueDecoder func(string) []byte

func decodePlainString(input string) []byte {
	return []byte(input)
}

func decodeBase64String(input string) []byte {
	outLen := base64.StdEncoding.DecodedLen(len(input))
	output := make([]byte, outLen)
	n, err := base64.StdEncoding.Decode(output, []byte(input))
	if err != nil {
		return nil
	}
	return output[:n]
}

var _ valueDecoder = decodePlainString
var _ valueDecoder = decodeBase64String
