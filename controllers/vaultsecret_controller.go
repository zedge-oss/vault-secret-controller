package controllers

import (
	"context"
	"reflect"

	"github.com/go-logr/logr"
	vault "github.com/hashicorp/vault/api"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"

	"gitlab.com/zedge/dd/vault-secret-controller/api/v1beta1"
	"gitlab.com/zedge/dd/vault-secret-controller/token"
)

// VaultSecretReconciler reconciles a VaultSecret object
type VaultSecretReconciler struct {
	client.Client
	Log          logr.Logger
	Scheme       *runtime.Scheme
	VaultConfig  *vault.Config
	AuthProvider token.Provider
}

// +kubebuilder:rbac:groups=zedge.io,resources=vaultsecrets,verbs=get;list;watch
// +kubebuilder:rbac:groups=zedge.io,resources=vaultsecrets/status,verbs=get;update;patch
// +kubebuilder:rbac:groups="",resources=secrets,verbs=get;list;watch;create;update

func (r *VaultSecretReconciler) Reconcile(req ctrl.Request) (ctrl.Result, error) {
	log := r.Log.WithValues("req", req.NamespacedName)
	var err error
	cli, err := r.newVaultClient()
	if err != nil {
		log.Error(err, "creating vault client")
		return ctrl.Result{Requeue: true}, err
	}
	vaultSecret := &v1beta1.VaultSecret{}
	if err = r.Get(context.Background(), req.NamespacedName, vaultSecret); err != nil {
		if errors.IsNotFound(err) {
			return ctrl.Result{}, nil
		}
		log.Error(err, "error reading VaultSecret", "name", req.NamespacedName)
		return ctrl.Result{Requeue: true}, err
	}
	var secret *vault.Secret
	switch {
	case vaultSecret.Spec.KeyValue != nil:
		path := vaultPathFromSpec(vaultSecret.Spec.KeyValue)
		log.Info("reading from vault", "path", path, "name", req.NamespacedName)
		secret, err = cli.Logical().Read(path)
	default:
		log.Info("no secret specified in VaultSecret", "name", req.NamespacedName)
	}
	if err != nil {
		if curl, ok := err.(*vault.OutputStringError); ok {
			log.Info("curl command", "command", curl.CurlString())
			return ctrl.Result{Requeue: false}, nil
		} else {
			log.Error(err, "error reading from vault", "name", req.NamespacedName)
			return ctrl.Result{Requeue: true}, err
		}
	}
	var kubeSecret corev1.Secret
	create := false
	update := false
	if err := r.Get(context.Background(), req.NamespacedName, &kubeSecret); err != nil {
		if errors.IsNotFound(err) {
			log.Info("will create", "name", req.NamespacedName)
			create = true
		} else {
			log.Error(err, "error reading Secret", "name", req.NamespacedName)
			return ctrl.Result{Requeue: true}, err
		}
	}
	newData := convertVaultData(secret)
	if kubeSecret.Data == nil || !reflect.DeepEqual(newData, kubeSecret.Data) || vaultSecret.Spec.Type != kubeSecret.Type {
		update = true
	}
	kubeSecret.Data = newData
	kubeSecret.Type = vaultSecret.Spec.Type
	if create {
		kubeSecret.Name = req.Name
		kubeSecret.Namespace = req.Namespace
		if err = controllerutil.SetControllerReference(vaultSecret, &kubeSecret, r.Scheme); err != nil {
			log.Error(err, "error setting controller reference", "name", kubeSecret.Name, "namespace", kubeSecret.Namespace)
			return ctrl.Result{}, err
		}
		log.Info("Creating Secret", "name", kubeSecret.Name, "namespace", kubeSecret.Namespace)
		err = r.Create(context.Background(), &kubeSecret)
	} else if update {
		log.Info("Updating Secret", "name", kubeSecret.Name, "namespace", kubeSecret.Namespace)
		err = r.Update(context.Background(), &kubeSecret)
	} else {
		log.Info("No changes needed", "name", kubeSecret.Name, "namespace", kubeSecret.Namespace)

	}
	if err != nil {
		log.Error(err, "error creating or updating Secret")
		return ctrl.Result{Requeue: true}, err
	}
	return ctrl.Result{}, nil
}

func (r *VaultSecretReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&v1beta1.VaultSecret{}).
		Owns(&corev1.Secret{}).
		Complete(r)
}

func (r *VaultSecretReconciler) newVaultClient() (*vault.Client, error) {
	vaultConfig := r.VaultConfig
	vaultClient, err := vault.NewClient(vaultConfig)
	if err != nil {
		return nil, err
	}
	clientToken, err := r.AuthProvider.GetToken()
	if err != nil {
		return nil, err
	}
	vaultClient.SetToken(clientToken)
	return vaultClient, nil
}
