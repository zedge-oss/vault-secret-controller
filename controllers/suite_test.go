package controllers

import (
	"context"
	"encoding/hex"
	"flag"
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"path/filepath"
	"testing"

	"github.com/go-logr/logr"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"

	vault "github.com/hashicorp/vault/api"
	"github.com/onsi/ginkgo/reporters"
	"github.com/ory/dockertest"
	corev1 "k8s.io/api/core/v1"
	rbacv1 "k8s.io/api/rbac/v1"
	extensionsv1beta1client "k8s.io/apiextensions-apiserver/pkg/client/clientset/clientset/typed/apiextensions/v1beta1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/envtest"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"

	zedgev1beta1 "gitlab.com/zedge/dd/vault-secret-controller/api/v1beta1"
	"gitlab.com/zedge/dd/vault-secret-controller/token"
	// +kubebuilder:scaffold:imports
)

// These tests use Ginkgo (BDD-style Go testing framework). Refer to
// http://onsi.github.io/ginkgo/ to learn more about Ginkgo.

const (
	vaultTestImage    = "vault"
	vaultTestImageTag = "1.2.4"
)

var (
	cfg                         *rest.Config
	ctx                         = context.Background()
	k8sClient                   client.Client
	vaultClient                 *vault.Client
	testEnv                     *envtest.Environment
	testLogger                  logr.Logger
	dockerConn                  *dockertest.Pool
	testVaultContainer          *dockertest.Resource
	vaultRootToken              string
	testVaultConfig             *vault.Config
	testTokenProvider           token.Provider
	testController              *VaultSecretReconciler
	testManager                 ctrl.Manager
	managerStopCh               chan struct{}
	testServiceAccountTokenPath string
	cleanupList                 = make([]runtime.Object, 0)
	vaultKubeApiServer          string
	useExistingCluster          bool
	runEndToEndTests            bool
)

func init() {
	flag.StringVar(&vaultKubeApiServer, "vault-k8s-server", "", "scheme://host:port for apiserver to use by vault (if running in a container alongside kind)")
	flag.BoolVar(&useExistingCluster, "use-existing-cluster", false, "whether to run tests in the current cluster context")
	flag.BoolVar(&runEndToEndTests, "run-e2e-tests", false, "whether to run end-to-end tests (requires --use-existing-cluster)")
}

func TestMain(m *testing.M) {
	flag.Parse()
	os.Exit(m.Run())
}

func TestAPIs(t *testing.T) {
	RegisterFailHandler(Fail)

	RunSpecsWithDefaultAndCustomReporters(t, "Controller Suite", []Reporter{
		envtest.NewlineReporter{},
		reporters.NewJUnitReporter("junit.xml"),
	})
}

var _ = BeforeSuite(func(done Done) {
	SetupTestLogger()
	SetupVaultServer()
	WaitForVaultServerToInitialize()
	SetupTestControlPlane()
	SetupController()

	// +kubebuilder:scaffold:scheme

	close(done)
}, 60)

var _ = AfterSuite(func() {
	By("cleaning up test resources")
	for _, resource := range cleanupList {
		_ = k8sClient.Delete(ctx, resource)
	}
	By("tearing down the test environment")
	Expect(deleteVaultSecretCRD()).To(Succeed())
	Expect(testEnv.Stop()).To(Succeed())
	Expect(dockerConn.Purge(testVaultContainer)).To(Succeed())
	_ = os.Remove(testServiceAccountTokenPath)
	//close(managerStopCh)
})

func SetupTestLogger() {
	By("setting up test logger")
	testLogger = zap.New(func(o *zap.Options) {
		o.Development = true
		o.DestWritter = GinkgoWriter
	})
	logf.SetLogger(testLogger)
}

func SetupTestControlPlane() {
	var err error
	By("bootstrapping kubernetes test environment", func() {
		testEnv = &envtest.Environment{
			CRDDirectoryPaths:  []string{filepath.Join("..", "config", "crd", "bases")},
			UseExistingCluster: &useExistingCluster,
		}
		cfg, err = testEnv.Start()
		Expect(err).ToNot(HaveOccurred())
		Expect(cfg).ToNot(BeNil())
		err = zedgev1beta1.AddToScheme(scheme.Scheme)
		Expect(err).ToNot(HaveOccurred())
		k8sClient, err = client.New(cfg, client.Options{Scheme: scheme.Scheme})
		Expect(err).ToNot(HaveOccurred())
		Expect(k8sClient).ToNot(BeNil())
	})

	if runEndToEndTests {
		var rnd, ns, authSecretToken, controllerJWT string
		var controllerServiceAccount *corev1.ServiceAccount
		tmp, err := ioutil.TempFile("", "token.*")
		Expect(err).ToNot(HaveOccurred())
		testServiceAccountTokenPath = tmp.Name()
		rnd = fmt.Sprintf("%04d", rand.Intn(10000))
		crb := "vault-auth-delegation-" + rnd
		authRole := "role-" + rnd
		policy := "policy-" + rnd

		By("creating a temporary namespace", func() {
			ns = "test-" + rnd
			testNs := &corev1.Namespace{ObjectMeta: metav1.ObjectMeta{Name: ns}}
			Expect(createAndQueueForCleanup(testNs)).To(Succeed())
		})

		By("creating a ServiceAccount for the controller", func() {
			controllerServiceAccount = &corev1.ServiceAccount{ObjectMeta: objectMeta("controller", ns)}
			Expect(createResource(controllerServiceAccount)).To(Succeed())
			controllerJWT = GetServiceAccountToken(controllerServiceAccount)
			Expect(ioutil.WriteFile(testServiceAccountTokenPath, []byte(controllerJWT), 0600)).To(Succeed())
		})

		By("creating a ServiceAccount and ClusterRoleBinding for vault authentication", func() {
			authServiceAccount := &corev1.ServiceAccount{ObjectMeta: objectMeta("vault-auth", ns)}
			Expect(createResource(authServiceAccount)).To(Succeed())
			clusterRoleBinding := &rbacv1.ClusterRoleBinding{
				ObjectMeta: objectMeta(crb),
				Subjects: []rbacv1.Subject{
					{Kind: "ServiceAccount", Name: authServiceAccount.Name, Namespace: authServiceAccount.Namespace, APIGroup: ""},
				},
				RoleRef: rbacv1.RoleRef{
					Kind:     "ClusterRole",
					Name:     "system:auth-delegator",
					APIGroup: "rbac.authorization.k8s.io",
				},
			}
			Expect(createAndQueueForCleanup(clusterRoleBinding)).To(Succeed())
			authSecretToken = GetServiceAccountToken(authServiceAccount)
		})

		By("configuring vault auth backend", func() {
			Expect(vaultClient).ToNot(BeNil())
			Expect(vaultClient.Sys().DisableAuth("kubernetes")).To(Succeed())
			authOpts := &vault.EnableAuthOptions{
				Type: "kubernetes",
				Config: vault.AuthConfigInput{
					DefaultLeaseTTL: "5s",
					MaxLeaseTTL:     "60s",
				},
				Options: map[string]string{
					"kubernetes_host": cfg.Host,
					//"kubernetes_host": "https://172.17.0.15:6443",
					"kubernetes_ca_cert": string(cfg.CAData),
					"token_reviewer_jwt": authSecretToken,
				},
			}
			testLogger.Info("EnableWithWithOptions", "options", *authOpts)
			Expect(vaultClient.Sys().EnableAuthWithOptions("kubernetes", authOpts)).To(Succeed())
			Expect(vaultClient.Sys().PutPolicy(policy, vaultTestPolicy)).To(Succeed())
			var authConfig *vault.Secret
			authConfig, err = vaultWrite("auth/kubernetes/role/"+authRole, map[string]interface{}{
				// bound_service_account_names=controller bound_service_account_namespaces="$ns" policies="$policy" ttl=5s
				"bound_service_account_names":      controllerServiceAccount.Name,
				"bound_service_account_namespaces": controllerServiceAccount.Namespace,
				"policies":                         policy,
				"ttl":                              "5s",
			})
			Expect(err).NotTo(HaveOccurred())
			testLogger.Info("wrote auth/kubernetes/config", "secret", authConfig)
		})

		By("testing vault kubernetes auth", func() {
			var resp *vault.Secret
			resp, err = vaultWrite("auth/kubernetes/login", map[string]interface{}{
				"jwt":  controllerJWT,
				"role": authRole,
			})
			testLogger.Info("testing vault kubernetes auth", "secret", resp, "error", err)
			Expect(err).ToNot(HaveOccurred())
		})

		By("setting up a token provider for the controller", func() {
			testTokenProvider = token.NewKubernetesProvider(
				testVaultConfig,
				testLogger,
				"auth/kubernetes/login",
				authRole,
				testServiceAccountTokenPath,
			)
		})
	} else {
		testTokenProvider = &token.StaticProvider{Token: vaultRootToken}
	}
}

func GetServiceAccountToken(sa *corev1.ServiceAccount) string {
	var secretRef corev1.ObjectReference
	Eventually(func() error {
		key := types.NamespacedName{Name: sa.Name, Namespace: sa.Namespace}
		tmp := &corev1.ServiceAccount{}
		if err := k8sClient.Get(ctx, key, tmp); err != nil {
			return err
		}
		if len(tmp.Secrets) < 1 {
			return fmt.Errorf("no secrets in ServiceAccount, this test needs to run with a full Kubernetes cluster")
		}
		secretRef = tmp.Secrets[0]
		return nil
	}, "2s", "100ms").Should(Succeed())
	var authSecretToken string
	Eventually(func() error {
		testLogger.Info("Fetching Secret", "name", secretRef.Name, "namespace", sa.Namespace)
		key := types.NamespacedName{Name: secretRef.Name, Namespace: sa.Namespace}
		tmp := &corev1.Secret{}
		if err := k8sClient.Get(ctx, key, tmp); err != nil {
			return err
		}
		data, ok := tmp.Data["token"]
		if !ok {
			return fmt.Errorf(`no "token" found in secret %q`, key)
		}
		authSecretToken = string(data)
		return nil
	}, "2s", "100ms").Should(Succeed())
	return authSecretToken
}

func objectMeta(name string, namespace ...string) metav1.ObjectMeta {
	obj := metav1.ObjectMeta{Name: name}
	if len(namespace) == 1 {
		obj.Namespace = namespace[0]
	}
	return obj
}

func createAndQueueForCleanup(resource runtime.Object) error {
	if err := createResource(resource); err != nil {
		return err
	}
	cleanupList = append(cleanupList, resource)
	return nil
}

func createResource(resource runtime.Object) error {
	if err := k8sClient.Create(ctx, resource); err != nil {
		return err
	}
	return nil
}

// If running in an existing cluster, delete the VaultSecret CRD
func deleteVaultSecretCRD() error {
	if useExistingCluster {
		cli, err := extensionsv1beta1client.NewForConfig(cfg)
		if err != nil {
			return err
		}
		return cli.CustomResourceDefinitions().Delete("vaultsecrets.zedge.io", &metav1.DeleteOptions{})
	}
	return nil
}

func vaultWrite(path string, data map[string]interface{}) (*vault.Secret, error) {
	logData := make([]interface{}, 2)
	logData[0] = "path"
	logData[1] = path
	if data != nil {
		logData = append(logData, "data", data)
	}
	secret, err := vaultClient.Logical().Write(path, data)
	if secret != nil {
		logData = append(logData, "response", *secret)
	}
	if err != nil {
		logData = append(logData, "error", err)
	}
	testLogger.Info("vault write", logData...)
	if err != nil {
		return nil, err
	}
	return secret, err
}

func SetupVaultServer() {
	By("starting vault server container")
	var err error
	dockerConn, err = dockertest.NewPool("")
	randBuf := make([]byte, 10)
	_, err = rand.Read(randBuf)
	Expect(err).NotTo(HaveOccurred())
	vaultRootToken = hex.EncodeToString(randBuf)
	cwd, _ := filepath.Abs(".")
	testVaultContainer, err = dockerConn.RunWithOptions(&dockertest.RunOptions{
		Repository:   vaultTestImage,
		Tag:          vaultTestImageTag,
		CapAdd:       []string{"IPC_LOCK"},
		Cmd:          []string{"server", "--dev", "--dev-root-token-id", vaultRootToken},
		ExposedPorts: []string{"8200/tcp"},
		Mounts:       []string{cwd + ":/hostcwd"}, // for audit logging
	})
	Expect(err).NotTo(HaveOccurred())
	testVaultConfig = &vault.Config{
		Address: "http://" + testVaultContainer.GetHostPort("8200/tcp"),
	}
}

func WaitForVaultServerToInitialize() {
	By("waiting for vault server to initialize")
	var err error
	vaultClient, err = vault.NewClient(testVaultConfig)
	Expect(err).ToNot(HaveOccurred())
	vaultClient.SetToken(vaultRootToken)
	Eventually(func() error {
		st, err := vaultClient.Sys().SealStatus()
		if err != nil {
			return err
		}
		if !st.Initialized {
			return fmt.Errorf("vault server not initialized")
		}
		return nil
	}, "5s", "200ms").Should(Succeed())
	Expect(vaultClient.Sys().EnableAuditWithOptions("file", &vault.EnableAuditOptions{
		Type: "file",
		Options: map[string]string{
			"path": "/hostcwd/vault-audit.log",
		}})).To(Succeed())
}

func SetupController() {
	By("starting vault secret controller")
	var err error
	testController = &VaultSecretReconciler{
		Client:       k8sClient,
		Log:          testLogger,
		Scheme:       scheme.Scheme,
		VaultConfig:  testVaultConfig,
		AuthProvider: testTokenProvider,
	}
	testManager, err = ctrl.NewManager(cfg, ctrl.Options{
		Scheme:             scheme.Scheme,
		MetricsBindAddress: "0",
		LeaderElection:     false,
		Port:               0,
	})
	Expect(err).NotTo(HaveOccurred())
	managerStopCh = make(chan struct{}, 1)
	Expect(testController.SetupWithManager(testManager)).To(Succeed())
	go func() {
		Expect(testManager.Start(managerStopCh)).To(Succeed())
	}()
}

const vaultTestPolicy = `
path "secret/metadata/test" {
  capabilities = ["list"]
}

path "secret/data/test/*" {
  capabilities = ["create", "read", "update", "delete"]
}
`
