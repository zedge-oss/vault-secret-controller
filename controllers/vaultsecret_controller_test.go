package controllers

import (
	"context"
	"encoding/base64"

	vault "github.com/hashicorp/vault/api"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/zedge/dd/vault-secret-controller/api/v1beta1"
)

const (
	testNamespace          = "ns"
	plainSecretName        = "plain-secret"
	encodedSecretName      = "encoded-secret"
	plainSecretKvPath      = "secret/test/plain"
	base64SecretKvPath     = "secret/test/base64"
	plainSecretDataPath    = "secret/data/test/plain"
	base64SecretDataPath   = "secret/data/test/base64"
	testKey                = "key"
	testValue              = "value"
	testValueBase64Encoded = "dmFsdWU="
)

var _ = Describe("VaultSecret controller", func() {
	var (
		err           error
		vaultSecret   *v1beta1.VaultSecret
		kubeSecret    *corev1.Secret
		kubeSecretKey types.NamespacedName
		ctx           = context.Background()
		secretName    string
		kvPath        string
	)
	BeforeEach(func() {
		var cli *vault.Client
		cli, err = testController.newVaultClient()
		Expect(err).ToNot(HaveOccurred())
		_, err = cli.Logical().Write(plainSecretDataPath, map[string]interface{}{
			"data": map[string]interface{}{
				testKey: testValue,
			},
		})
		Expect(err).ToNot(HaveOccurred())
		b64Val := make([]byte, base64.StdEncoding.EncodedLen(len(testValue)))
		base64.StdEncoding.Encode(b64Val, []byte(testValue))
		Expect(err).ToNot(HaveOccurred())
		_, err = cli.Logical().Write(base64SecretDataPath, map[string]interface{}{
			"data": map[string]interface{}{
				EncodingSecretKeyName: Base64Encoding,
				testKey:               string(b64Val),
			},
		})
		Expect(err).ToNot(HaveOccurred())
	})
	JustBeforeEach(func() {
		vaultSecret = &v1beta1.VaultSecret{
			ObjectMeta: metav1.ObjectMeta{Namespace: testNamespace, Name: secretName},
			Spec: v1beta1.VaultSecretSpec{
				KeyValue: &v1beta1.VaultKeyValue{Path: kvPath},
			},
		}
		kubeSecretKey = types.NamespacedName{Namespace: testNamespace, Name: secretName}
		kubeSecret = &corev1.Secret{}
		Expect(k8sClient.Create(ctx, vaultSecret)).To(Succeed())
	})
	When("creating a plain VaultSecret", func() {
		BeforeEach(func() {
			secretName = plainSecretName
			kvPath = plainSecretKvPath
		})
		It("it creates a matching Secret", func() {
			Eventually(func() error {
				return k8sClient.Get(context.Background(), kubeSecretKey, kubeSecret)
			}, "5s", "200ms").Should(Succeed())
			Expect(kubeSecret.Data).To(HaveKey(testKey))
			Expect(kubeSecret.Data[testKey]).To(Equal([]byte(testValue)))
			Expect(kubeSecret.Data).To(HaveLen(1))
		})
	})
	When("creating a base64-encoded VaultSecret", func() {
		BeforeEach(func() {
			secretName = encodedSecretName
			kvPath = base64SecretKvPath
		})
		It("it creates a matching Secret", func() {
			Eventually(GettingResource(kubeSecretKey, kubeSecret)).Should(Succeed())
			Expect(kubeSecret.Data).ToNot(HaveKey(EncodingSecretKeyName))
			Expect(kubeSecret.Data).To(HaveKey(testKey))
			Expect(kubeSecret.Data[testKey]).To(Equal([]byte(testValue)))
			Expect(kubeSecret.Data).To(HaveLen(1))
		})
	})
})

func GettingResource(key client.ObjectKey, obj runtime.Object) func() error {
	return func() error {
		return k8sClient.Get(context.Background(), key, obj)
	}
}
