package controllers

import (
	"encoding/base64"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	vault "github.com/hashicorp/vault/api"

	"gitlab.com/zedge/dd/vault-secret-controller/api/v1beta1"
)

var _ = Describe("Utility functions", func() {
	When("converting a vault secret to map", func() {
		var secret *vault.Secret
		var kubeSecretData map[string][]byte
		BeforeEach(func() {
			secret = &vault.Secret{
				Data: map[string]interface{}{
					"data": map[string]interface{}{
						"foo": "foo contents",
						"bar": "bar contents",
					},
				},
			}
			kubeSecretData = convertVaultData(secret)
		})
		It("should be converted correctly", func() {
			Expect(kubeSecretData).To(Equal(map[string][]byte{
				"foo": []byte("foo contents"),
				"bar": []byte("bar contents"),
			}))
		})
	})
	When("converting a vault secret with base64-encoded values", func() {
		var kubeSecretData map[string][]byte
		BeforeEach(func() {
			kubeSecretData = convertVaultData(&vault.Secret{
				Data: map[string]interface{}{
					"data": map[string]interface{}{
						"__encoding": "base64",
						"foo":        toBase64("foo contents"),
						"bar":        toBase64("bar contents"),
					},
				},
			})
		})
		It("should be decoded correctly", func() {
			Expect(kubeSecretData).To(Equal(map[string][]byte{
				"foo": []byte("foo contents"),
				"bar": []byte("bar contents"),
			}))
		})
	})
	When("resolving a kv path", func() {
		var kv *v1beta1.VaultKeyValue
		Context("without mount", func() {
			BeforeEach(func() {
				kv = &v1beta1.VaultKeyValue{Path: "secret/foo"}
			})
			It("should have its mount taken from the first path element", func() {
				Expect(vaultPathFromSpec(kv)).To(Equal("secret/data/foo"))
			})
		})
		Context("with mount", func() {
			BeforeEach(func() {
				kv = &v1beta1.VaultKeyValue{Path: "bar", Mount: "secret2"}
			})
			It("should have its mount used", func() {
				Expect(vaultPathFromSpec(kv)).To(Equal("secret2/data/bar"))
			})
		})
	})
})

func toBase64(str string) string {
	data := make([]byte, base64.StdEncoding.EncodedLen(len(str)))
	base64.StdEncoding.Encode(data, []byte(str))
	return string(data)
}
